﻿using System;
using System.Runtime.Versioning;

namespace ConsoleGameSquares
{
    [SupportedOSPlatform("windows")]
    public class Player
    {
        public string Name { get; set; }
        public int Score { get; private set; }
        public int Move { get; private set; }
        private Area Area { get; set; }
        private Random randomGenerator;
        public int FirstCube { get; set; }
        public int SecondCube { get; set; }
        public FieldDots Symbol { get; private set; }

        public Player(string name, int move, FieldDots symbol)
        {
            this.Name = name;
            this.Score = 0;
            this.Move = move;
            this.Area = null;
            this.Symbol = symbol;
        }

        public void DecreasingMove()
        {
            Move--;
        }

        private void CalculateScore()
        {
            this.Score += this.FirstCube * this.SecondCube;
        }

        public void RollTheDice()
        {
            if (randomGenerator == null)
            {
                this.randomGenerator = new Random();
            }
            this.FirstCube = randomGenerator.Next(1, 7);
            this.SecondCube = randomGenerator.Next(1, 7);
        }

        private void SelectPositionToInsertArea()
        {
            if (Area == null)
            {
                this.Area = new Area();
            }
            Console.WriteLine("Координата по ширине -> X:");
            int x = Game.InputNumber();
            Console.WriteLine("Координата по высоте -> Y:");
            int y = Game.InputNumber();
            this.Area.CreateStartPoint(x, y);
            this.Area.CreateArea(this.FirstCube - 1, this.SecondCube - 1);
        }

        internal void InsertArea(FieldDots[,] gameField)
        {
            bool skip = false;
            SelectPositionToInsertArea();
            while (!(IsAreaInsertInGameField(gameField) && IsAreaOverlaps(gameField, out skip)))
            {
                if (skip)
                {
                    return;
                }
                Console.WriteLine("Повторяем ввод начальной точки.");
                SelectPositionToInsertArea();
            }
            CalculateScore();
            for (int i = this.Area.StartPoint.Y; i <= this.Area.EndPoint.Y; i++)
            {
                for (int j = this.Area.StartPoint.X; j <= this.Area.EndPoint.X; j++)
                {
                    gameField[i, j] = this.Symbol;
                }
            }
        }
        private bool IsAreaOverlaps(FieldDots[,] gameField, out bool skip)
        {
            for (int i = this.Area.StartPoint.Y; i <= this.Area.EndPoint.Y; i++)
            {
                for (int j = this.Area.StartPoint.X; j <= this.Area.EndPoint.X; j++)
                {
                    if (gameField[i, j] != FieldDots.FreeSpace)
                    {
                        Console.WriteLine("Ваша область не влазит.");
                        Console.WriteLine("Продолжить ввод пар XY (n) или пропустить ход (y)? y/n");
                        if (Game.InputSymbol())
                        {
                            skip = true;
                            return false;
                        }
                        else
                        {
                            skip = false;
                            return false;
                        }
                    }
                }
            }
            skip = false;
            return true;
        }

        private bool IsAreaInsertInGameField(FieldDots[,] gameField)
        {
            if (Area == null)
            {
                return false;
            }

            if (!(this.Area.StartPoint.Y < gameField.GetLength(0) - 1 && this.Area.StartPoint.Y > 0))
            {
                Console.WriteLine("Выбранная координата Y выходит за границы.");
                return false;
            }

            if (!(this.Area.StartPoint.X < gameField.GetLength(1) - 1 && this.Area.StartPoint.X > 0))
            {
                Console.WriteLine("Выбранная координата X выходит за границы.");
                return false;
            }

            if (!(this.Area.EndPoint.Y < gameField.GetLength(0) - 1 && this.Area.EndPoint.Y > 0))
            {
                Console.WriteLine("Вставляемая область выходит за границы игрового поля по Y.");
                return false;
            }

            if (!(this.Area.EndPoint.X < gameField.GetLength(1) - 1 && this.Area.EndPoint.X > 0))
            {
                Console.WriteLine("Вставляемая область выходит за границы игрового поля по X.");
                return false;
            }

            return true;
        }
    }
}
