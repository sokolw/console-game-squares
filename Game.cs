﻿using System;
using System.Collections.Generic;
using System.Runtime.Versioning;

namespace ConsoleGameSquares
{
    [SupportedOSPlatform("windows")]
    class Game
    {
        private int Width { get; set; }
        private int Height { get; set; }
        private FieldDots[,] GameField { get; set; }
        private readonly List<Player> players;

        public Game()
        {
            this.Width = 32;
            this.Height = 22;
            this.players = new List<Player>();
        }

        public void Prepare()
        {
            Console.WriteLine("Настройка игры!");
            SetFieldSize();
            InitGameField();
            InitPlayers();
        }

        public void Start()
        {
            ReflectArea();
            while (IsGameContinue())
            {
                for (int i = 0; i < players.Count; i++)
                {
                    players[i].DecreasingMove();
                    Console.WriteLine($"Ход игрока №{i + 1}. Счет: {players[i].Score}. Осталось ходов: {players[i].Move}");
                    Console.WriteLine($"Фигурка игрока {(players[i].Symbol == FieldDots.Dollar ? "$" : "%")}");
                    Console.WriteLine("Бросок костей!");
                    players[i].RollTheDice();
                    Console.WriteLine($"Выпали кости [{players[i].FirstCube}][{players[i].SecondCube}]");
                    Console.WriteLine("Введите координаты для вставки области.");
                    players[i].InsertArea(GameField);
                    Console.Clear();
                    ReflectArea();
                }
            }
            if (players[0].Score > players[1].Score)
            {
                Console.WriteLine($"Выиграл {players[0].Name} со счетом: {players[0].Score}");
            }
            else
            {
                Console.WriteLine($"Выиграл {players[1].Name} со счетом: {players[1].Score}");
            }
        }

        private void SetFieldSize()
        {
            Console.WriteLine("Выбрать размер игрового поля? (Да 1 | 0 Нет)");
            if (InputNumber() > 0)
            {
                int temp;
                Console.WriteLine("Минимальный размер - ширина: 30, высота: 20");
                Console.WriteLine("Максимальный размер - ширина: {0}, высота: {1}", 66, Console.LargestWindowHeight);
                Console.WriteLine("Ширина:");
                temp = InputNumber();
                while (!IsValidNumberInRange(temp, 30, 66))
                {
                    Console.WriteLine("Введите число от 30 до {0}", 66);
                    temp = InputNumber();
                }
                this.Width = temp + 2;
                Console.WriteLine("Высота:");
                temp = InputNumber();
                while (!IsValidNumberInRange(temp, 20, Console.LargestWindowHeight))
                {
                    Console.WriteLine("Введите число от 20 до {0}", Console.LargestWindowHeight);
                    temp = InputNumber();
                }
                this.Height = temp + 2;
                ChangeConsoleSize();
            }
            else
            {
                ChangeConsoleSize();
            }

            GameField = new FieldDots[Height, Width];
        }

        private void ChangeConsoleSize()
        {
            Console.Clear();
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
        }

        private bool IsValidNumberInRange(int number, int min, int max)
        {
            return number >= min && number <= max;
        }

        public static int InputNumber()
        {
            if (int.TryParse(Console.ReadLine(), out int result))
            {
                return result;
            }
            else
            {
                Console.WriteLine("Введите число!");
                return InputNumber();
            }
        }

        public static bool InputSymbol()
        {
            string temp = Console.ReadLine();
            if (temp.Equals("y", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            else if (temp.Equals("n", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }
            else
            {
                Console.WriteLine("Введите y/n !");
                return InputSymbol();
            }
        }

        private void InitPlayers()
        {
            int maxPlayers = 2;
            int move = SetMoves();
            for (int i = 1; i <= maxPlayers; i++)
            {
                players.Add(new Player($"Player {i}", move, (FieldDots)i + 1));
            }
        }

        private int SetMoves()
        {
            Console.WriteLine("Введите количество ходов, минимальное число 20.");
            Console.WriteLine("Рекомендуется ходов для выбранного поля {0}", ((Width - 2) * (Height - 2)) / 30);
            int temp = InputNumber();
            while (!IsValidNumberInRange(temp, 20, (((Width - 2) * (Height - 2)) / 30) + 100))
            {
                Console.WriteLine("Введите число от 20 до {0} и так много ходов", (((Width - 2) * (Height - 2)) / 30) + 100);
                temp = InputNumber();
            }
            return temp;
        }

        private void InitGameField()
        {
            InitPerimeter();
        }

        private void InitPerimeter()
        {
            //top border
            for (int i = 0; i < GameField.GetLength(1); i++)
            {
                GameField[0, i] = FieldDots.Wall;
            }
            //bottom border
            for (int i = 0; i < GameField.GetLength(1); i++)
            {
                GameField[GameField.GetLength(0) - 1, i] = FieldDots.Wall;
            }
            //left border
            for (int i = 0; i < GameField.GetLength(0); i++)
            {
                GameField[i, 0] = FieldDots.Wall;
            }
            //right border
            for (int i = 0; i < GameField.GetLength(0); i++)
            {
                GameField[i, GameField.GetLength(1) - 1] = FieldDots.Wall;
            }
        }



        private bool IsGameContinue()
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].Move > 0)
                {
                    return true;
                }
            }
            return false;
        }

        private void ReflectArea()
        {
            Renderer.DrawField(GameField);
        }
    }
}
