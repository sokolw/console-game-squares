﻿using System;
using System.Runtime.Versioning;

namespace ConsoleGameSquares
{
    [SupportedOSPlatform("windows")]
    public static class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            game.Prepare();
            game.Start();
        }
    }
}
