﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleGameSquares
{
    public static class Renderer
    {
        public static void DrawField(FieldDots[,] GameField)
        {
            for (int i = 0; i < GameField.GetLength(0); i++)
            {
                for (int j = 0; j < GameField.GetLength(1); j++)
                {
                    //top numbers
                    if (i == 0 && j != 0 && j < GameField.GetLength(1) - 1)
                    {
                        if (j > 9)
                        {
                            Console.Write($"{j,3}");
                        }
                        else
                        {
                            Console.Write($"{j,2}");
                        }
                    }
                    //bottom numbers
                    else if (i == GameField.GetLength(0) - 1 && j != 0 && j < GameField.GetLength(1) - 1)
                    {
                        if (j > 9)
                        {
                            Console.Write($"{j,3}");
                        }
                        else
                        {
                            Console.Write($"{j,2}");
                        }
                    }
                    //left numbers
                    else if (j == 0 && i != 0 && i < GameField.GetLength(0) - 1)
                    {
                        Console.Write($"{i,2}");
                    }
                    //right numbers
                    else if (j == GameField.GetLength(1) - 1 && i != 0 && i < GameField.GetLength(0) - 1)
                    {
                        Console.Write($"{i,2}");
                    }
                    else if (i == 0 && j == 0)
                    {
                        Console.Write($"{"*",2}");
                    }
                    else if (i == 0 && j == GameField.GetLength(1) - 1)
                    {
                        Console.Write($"{"*",2}");
                    }
                    else if (i == GameField.GetLength(0) - 1 && j == 0)
                    {
                        Console.Write($"{"*",2}");
                    }
                    else if (i == GameField.GetLength(0) - 1 && j == GameField.GetLength(1) - 1)
                    {
                        Console.Write($"{"*",2}");
                    }
                    else
                    {
                        if (GameField[i, j] == FieldDots.FreeSpace || GameField[i, j] == FieldDots.Wall)
                        {
                            if (j > 9)
                            {
                                Console.Write($"{".",3}");
                            }
                            else
                            {
                                Console.Write($"{".",2}");
                            }
                        }
                        else
                        {
                            switch (GameField[i, j])
                            {
                                case FieldDots.Dollar:
                                    if (j > 9)
                                    {
                                        Console.Write($"{"$",3}");
                                    }
                                    else
                                    {
                                        Console.Write($"{"$",2}");
                                    }
                                    break;
                                case FieldDots.Percent:
                                    if (j > 9)
                                    {
                                        Console.Write($"{"%",3}");
                                    }
                                    else
                                    {
                                        Console.Write($"{"%",2}");
                                    }
                                    break;
                            }
                        }
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
