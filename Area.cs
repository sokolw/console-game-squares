﻿namespace ConsoleGameSquares
{
    class Area
    {
        public Point StartPoint { get; set; }
        public Point EndPoint { get; set; }

        public void CreateStartPoint(int x, int y)
        {
            if(this.StartPoint == null)
            {
                this.StartPoint = new Point(x, y);
            }
            else
            {
                this.StartPoint.X = x;
                this.StartPoint.Y = y;
            }
            
        }

        public void CreateArea(int firstCube, int secondCube)
        {
            if (this.EndPoint == null)
            {
                this.EndPoint = new Point(StartPoint.X + firstCube, StartPoint.Y + secondCube);
            }
            else
            {
                this.EndPoint.X = this.StartPoint.X + firstCube;
                this.EndPoint.Y = this.StartPoint.Y + secondCube;
            }
        }
    }
}
